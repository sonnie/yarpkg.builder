"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.makeConfig = exports.findTsconfig = void 0;
const tslib_1 = require("tslib");
const fslib_1 = require("@yarnpkg/fslib");
const fork_ts_checker_webpack_plugin_1 = tslib_1.__importDefault(require("fork-ts-checker-webpack-plugin"));
const webpack_merge_1 = tslib_1.__importDefault(require("webpack-merge"));
const webpack_1 = tslib_1.__importDefault(require("webpack"));
const identity = (value) => value;
// fork-ts-checker-webpack-plugin doesn't search
// for tsconfig.json files outside process.cwd() :(
function findTsconfig() {
    let nextTsContextRoot = process.cwd();
    let currTsContextRoot = null;
    while (nextTsContextRoot !== currTsContextRoot) {
        currTsContextRoot = nextTsContextRoot;
        nextTsContextRoot = fslib_1.npath.dirname(currTsContextRoot);
        if (fslib_1.xfs.existsSync(fslib_1.ppath.join(fslib_1.npath.toPortablePath(currTsContextRoot), `tsconfig.json`))) {
            break;
        }
    }
    if (nextTsContextRoot === currTsContextRoot)
        throw new Error(`No tsconfig.json files could be found`);
    return fslib_1.npath.join(currTsContextRoot, `tsconfig.json`);
}
exports.findTsconfig = findTsconfig;
// We don't want to add the BuildPlugin outside of our infra.
function getBuildPlugin() {
    if (!process.env.BUILD_MONITORING_ENABLED)
        return [];
    const BuildPlugin = require(`@datadog/build-plugin/dist/webpack`).BuildPlugin;
    return [new BuildPlugin({
            context: fslib_1.npath.join(process.cwd(), `../..`),
            datadog: {
                endPoint: `app.datadoghq.eu`,
                apiKey: process.env.DD_API_KEY,
                prefix: `webpack`,
                tags: [
                    `branchname:${process.env.GITHUB_REF || `branch`}`,
                    `sha:${process.env.GITHUB_SHA || `local`}`,
                    `jobname:${process.env.GITHUB_EVENT_NAME || `job`}`,
                    `ci:${process.env.CI ? 1 : 0}`,
                ],
            },
        })];
}
const makeConfig = (config) => webpack_merge_1.default(identity({
    mode: `none`,
    devtool: false,
    target: `node10.19`,
    node: {
        __dirname: false,
        __filename: false,
    },
    output: {
        libraryTarget: `commonjs2`,
    },
    resolve: {
        extensions: [`.mjs`, `.js`, `.ts`, `.tsx`, `.json`],
    },
    module: {
        rules: [{
                test: /\.m?js/,
                resolve: {
                    fullySpecified: false,
                },
            }, {
                test: /\.tsx?$/,
                exclude: /\.d\.ts$/,
                use: [{
                        loader: require.resolve(`babel-loader`),
                    }, {
                        loader: require.resolve(`ts-loader`),
                        options: identity({
                            compilerOptions: {
                                declaration: false,
                                module: `ESNext`,
                                moduleResolution: `node`,
                            },
                            onlyCompileBundledFiles: true,
                            transpileOnly: true,
                        }),
                    }],
            }],
    },
    externals: {
        // Both of those are native dependencies of text-buffer we can't bundle
        [`fs-admin`]: `{}`,
        [`pathwatcher`]: `{}`,
    },
    plugins: [
        ...getBuildPlugin(),
        new webpack_1.default.IgnorePlugin({
            resourceRegExp: /^encoding$/,
            contextRegExp: /node-fetch/,
        }),
        new webpack_1.default.DefinePlugin({ [`IS_WEBPACK`]: `true` }),
        new webpack_1.default.optimize.LimitChunkCountPlugin({ maxChunks: 1 }),
        new fork_ts_checker_webpack_plugin_1.default({
            typescript: {
                configFile: findTsconfig(),
            },
        }),
    ],
}), config);
exports.makeConfig = makeConfig;
