import webpack from 'webpack';
export declare type WebpackPlugin = ((this: webpack.Compiler, compiler: webpack.Compiler) => void) | webpack.WebpackPluginInstance;
export declare function findTsconfig(): import("@yarnpkg/fslib").NativePath;
export declare const makeConfig: (config: webpack.Configuration) => webpack.Configuration;
