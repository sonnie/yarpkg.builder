export declare function findPlugins({ basedir, profile, plugins }: {
    basedir: string;
    profile: string;
    plugins: Array<string>;
}): string[];
