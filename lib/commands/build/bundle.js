"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const cli_1 = require("@yarnpkg/cli");
const core_1 = require("@yarnpkg/core");
const esbuild_plugin_pnp_1 = require("@yarnpkg/esbuild-plugin-pnp");
const fslib_1 = require("@yarnpkg/fslib");
const chalk_1 = tslib_1.__importDefault(require("chalk"));
const child_process_1 = tslib_1.__importDefault(require("child_process"));
const clipanion_1 = require("clipanion");
const esbuild_wasm_1 = require("esbuild-wasm");
const fs_1 = tslib_1.__importDefault(require("fs"));
const module_1 = require("module");
const path_1 = tslib_1.__importDefault(require("path"));
const semver_1 = tslib_1.__importDefault(require("semver"));
const util_1 = require("util");
const findPlugins_1 = require("../../tools/findPlugins");
const execFile = util_1.promisify(child_process_1.default.execFile);
const pkgJsonVersion = (basedir) => {
    return require(`${basedir}/package.json`).version;
};
const suggestHash = async (basedir) => {
    try {
        const unique = await execFile(`git`, [`show`, `-s`, `--pretty=format:%ad.%t`, `--date=short`], { cwd: basedir });
        return `git.${unique.stdout.trim().replace(/-/g, ``).replace(`.`, `.hash-`)}`;
    }
    catch (_a) {
        return null;
    }
};
// eslint-disable-next-line arca/no-default-export
class BuildBundleCommand extends clipanion_1.Command {
    constructor() {
        super(...arguments);
        this.profile = clipanion_1.Option.String(`--profile`, `standard`, {
            description: `Only include plugins that are part of the the specified profile`,
        });
        this.plugins = clipanion_1.Option.Array(`--plugin`, [], {
            description: `An array of plugins that should be included besides the ones specified in the profile`,
        });
        this.noGitHash = clipanion_1.Option.Boolean(`--no-git-hash`, false, {
            description: `Don't include the git hash of the current commit in bundle version`,
        });
        this.noMinify = clipanion_1.Option.Boolean(`--no-minify`, false, {
            description: `Build a bundle for development, without optimizations (minifying, mangling, treeshaking)`,
        });
        this.sourceMap = clipanion_1.Option.Boolean(`--source-map`, false, {
            description: `Includes a source map in the bundle`,
        });
    }
    async execute() {
        const basedir = process.cwd();
        const portableBaseDir = fslib_1.npath.toPortablePath(basedir);
        const configuration = core_1.Configuration.create(portableBaseDir);
        const plugins = findPlugins_1.findPlugins({ basedir, profile: this.profile, plugins: this.plugins.map(plugin => path_1.default.resolve(plugin)) });
        const modules = [...cli_1.getDynamicLibs().keys()].concat(plugins);
        const output = path_1.default.join(basedir, `bundles/yarn.js`);
        let version = pkgJsonVersion(basedir);
        const hash = !this.noGitHash
            ? await suggestHash(basedir)
            : null;
        if (hash !== null)
            version = semver_1.default.prerelease(version) !== null
                ? `${version}.${hash}`
                : `${version}-${hash}`;
        const report = await core_1.StreamReport.start({
            configuration,
            includeFooter: false,
            stdout: this.context.stdout,
            forgettableNames: new Set([core_1.MessageName.UNNAMED]),
        }, async (report) => {
            await report.startTimerPromise(`Building the CLI`, async () => {
                const valLoad = (p, values) => {
                    const fn = require(p.replace(/.ts$/, `.val.js`));
                    return fn(values).code;
                };
                const valLoader = {
                    name: `val-loader`,
                    setup(build) {
                        build.onLoad({ filter: /[\\/]getPluginConfiguration\.ts$/ }, async (args) => ({
                            contents: valLoad(args.path, { modules, plugins }),
                            loader: `default`,
                        }));
                    },
                };
                const res = await esbuild_wasm_1.build({
                    banner: {
                        js: `#!/usr/bin/env node\n/* eslint-disable */\n//prettier-ignore`,
                    },
                    entryPoints: [path_1.default.join(basedir, `sources/cli.ts`)],
                    bundle: true,
                    define: { YARN_VERSION: JSON.stringify(version) },
                    outfile: output,
                    logLevel: `silent`,
                    plugins: [valLoader, esbuild_plugin_pnp_1.pnpPlugin()],
                    minify: !this.noMinify,
                    sourcemap: this.sourceMap ? `inline` : false,
                    target: `node12`,
                });
                for (const warning of res.warnings) {
                    if (warning.location !== null)
                        continue;
                    report.reportWarning(core_1.MessageName.UNNAMED, warning.text);
                }
                for (const warning of res.warnings) {
                    if (warning.location === null)
                        continue;
                    report.reportWarning(core_1.MessageName.UNNAMED, `${warning.location.file}:${warning.location.line}:${warning.location.column}`);
                    report.reportWarning(core_1.MessageName.UNNAMED, `   ↳ ${warning.text}`);
                }
            });
        });
        report.reportSeparator();
        const Mark = core_1.formatUtils.mark(configuration);
        if (report.hasErrors()) {
            report.reportError(core_1.MessageName.EXCEPTION, `${Mark.Cross} Failed to build the CLI`);
        }
        else {
            report.reportInfo(null, `${Mark.Check} Done building the CLI!`);
            report.reportInfo(null, `${Mark.Question} Bundle path: ${core_1.formatUtils.pretty(configuration, output, core_1.formatUtils.Type.PATH)}`);
            report.reportInfo(null, `${Mark.Question} Bundle size: ${core_1.formatUtils.pretty(configuration, fs_1.default.statSync(output).size, core_1.formatUtils.Type.SIZE)}`);
            report.reportSeparator();
            const basedirReq = module_1.createRequire(`${basedir}/package.json`);
            for (const plugin of plugins) {
                const { name } = basedirReq(`${plugin}/package.json`);
                report.reportInfo(null, `${chalk_1.default.yellow(`→`)} ${core_1.structUtils.prettyIdent(configuration, core_1.structUtils.parseIdent(name))}`);
            }
        }
        return report.exitCode();
    }
}
exports.default = BuildBundleCommand;
BuildBundleCommand.paths = [
    [`build`, `bundle`],
];
BuildBundleCommand.usage = clipanion_1.Command.Usage({
    description: `build the local bundle`,
    details: `
      This command builds the local bundle - the Yarn binary file that is installed in projects.
    `,
    examples: [[
            `Build the local bundle`,
            `$0 build bundle`,
        ], [
            `Build the local development bundle`,
            `$0 build bundle --no-minify`,
        ]],
});
