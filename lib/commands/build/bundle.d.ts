import { Command, Usage } from 'clipanion';
export default class BuildBundleCommand extends Command {
    static paths: string[][];
    static usage: Usage;
    profile: string;
    plugins: string[];
    noGitHash: boolean;
    noMinify: boolean;
    sourceMap: boolean;
    execute(): Promise<0 | 1>;
}
